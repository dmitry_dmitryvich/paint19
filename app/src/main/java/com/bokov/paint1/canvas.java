package com.bokov.paint1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class canvas extends AppCompatActivity implements OnClickListener {
    private CanvasView canvasView;
    private ImageButton btnSave;
    private Button btnChooseImage;

    private static final int SELECT_PHOTO = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        int tmplName = bundle.getInt("tmpl");
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), tmplName);

        BitmapDrawable bg = new BitmapDrawable(getResources(), bitmap);
        if (Build.VERSION.SDK_INT >= 16) {
            canvasView.setBackground(bg);
        } else {
            canvasView.setBackgroundDrawable(bg);
        }
    }

    public void clearCanvas(View v) {
        canvasView.clearCanvas();
    }

    public void setEraser(View v) {
        canvasView.setEraser();
    }

    public void setColPencilBlack(View v) {
        canvasView.setColPencilBlack();
    }

    public void setColPencilRed(View v) {
        canvasView.setColPencilRed();
    }

    public void setColPencilBlue(View v) {
        canvasView.setColPencilBlue();
    }

    public void setColPencilYellow(View v) {
        canvasView.setColPencilYellow();
    }

    public void setColPencilGreen(View v) {
        canvasView.setColPencilGreen();
    }

    public void setColPencilGray(View v) {
        canvasView.setColPencilGray();
    }

    @Override
    public void onClick(View v) {
        if (v == btnSave) {
            saveImage();
        } else if (v == btnChooseImage) {
            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
            photoPickerIntent.setType("image/*");
            startActivityForResult(photoPickerIntent, SELECT_PHOTO);
        }
    }

    public File saveImage() {
        canvasView.setDrawingCacheEnabled(true);
        Bitmap bm = canvasView.getDrawingCache();

        File fPath = Environment.getExternalStorageDirectory();

        File f = null;

        f = new File(fPath, UUID.randomUUID().toString() + ".png");

        try {
            FileOutputStream strm = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 100, strm);
            strm.close();

            Toast.makeText(getApplicationContext(), "Image is saved.", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if (resultCode == RESULT_OK) {
            Uri selectedImage = imageReturnedIntent.getData();
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(selectedImage);
                Bitmap bitmap = BitmapFactory.decodeStream(imageStream);

                BitmapDrawable bg = new BitmapDrawable(getResources(), bitmap);

                if (Build.VERSION.SDK_INT >= 16) {
                    canvasView.setBackground(bg);
                } else {
                    canvasView.setBackgroundDrawable(bg);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
}