package com.bokov.paint1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class CanvasView extends View {

    public int width;
    public int height;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mPaint;
    private float mX, mY;
    private static final float TOLERANCE = 5;
    Context context;
    private Paint eraserPaint;

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);

        onCanvasInitialization();
    }

    public void onCanvasInitialization(){
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(4);
        mCanvas = new Canvas(mBitmap);
        Paint newPaint = new Paint(mPaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(mPath, mPaint);
    }

    private void startTouch(float x, float y){
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void moveTouch(float x, float y){
        float dx = Math.abs(x-mX);
        float dy = Math.abs(y-mY);
        if(dx >= TOLERANCE || dy >= TOLERANCE){
            mPath.quadTo(mX, mY, (x+mX)/2, (y+mY)/2);
            mX = x;
            mY = y;
        }
    }

    public  void clearCanvas(){
        mPath.reset();
        invalidate();
    }

    public void setEraser(){
        mPaint.setColor(Color.TRANSPARENT);
        mPaint.setStrokeWidth(10);
        Paint newPaint = new Paint(mPaint);
    }

    public void setColPencilBlack(){
        mPaint.setColor(Color.BLACK);
        mPaint.setStrokeWidth(4);
        Paint newPaint = new Paint(mPaint);
    }

    public void setColPencilRed(){
        mPaint.setColor(Color.RED);
        mPaint.setStrokeWidth(4);
        Paint newPaint = new Paint(mPaint);
    }

    public void setColPencilBlue(){
        mPaint.setColor(Color.BLUE);
        mPaint.setStrokeWidth(4);
        Paint newPaint = new Paint(mPaint);
    }

    public void setColPencilGreen(){
        mPaint.setColor(Color.GREEN);
        mPaint.setStrokeWidth(4);
        Paint newPaint = new Paint(mPaint);
    }

    public void setColPencilYellow(){
        mPaint.setColor(Color.YELLOW);
        mPaint.setStrokeWidth(4);
        Paint newPaint = new Paint(mPaint);
    }

    public void setColPencilGray(){
        mPaint.setColor(Color.GRAY);
        mPaint.setStrokeWidth(4);
        Paint newPaint = new Paint(mPaint);
    }

    private void upTouch(){
        mPath.lineTo(mX, mY);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }
}