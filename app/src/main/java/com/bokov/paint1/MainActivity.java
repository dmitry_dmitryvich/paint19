package com.bokov.paint1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity  {
    private ImageButton showButterflyImage;
    private ImageButton showFaceImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showButterflyImage = findViewById(R.id.showButterflyImage);
        showButterflyImage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), canvas.class);
                intent.putExtra("tmpl", R.drawable.butter);
                startActivity(intent);
            }
        });

//        showFaceImage = findViewById(R.id.showFaceImage);
//        OnClickListener face = new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent("com.bokov.paint1.canvas");
//                intent.putExtra("tmpl", R.drawable.face);
//                startActivity(intent);
//            }
//        };
//        showFaceImage.setOnClickListener(face);
    }

//    @Override
//    public void onClick(View view) {
//        Intent intent = new Intent(this, canvas.class);
//        if (view == showButterflyImage) {
//            intent.putExtra("tmpl", R.drawable.butter);
//            startActivity(intent);
//        } else if (view == showFaceImage) {
//            intent.putExtra("tmpl", R.drawable.face);
//            startActivity(intent);
//        }
//    }

}